mhwaveedit (1.4.24-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Wrap long lines in changelog entries: 1.4.7-1.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 26 Mar 2020 10:03:05 +0000

mhwaveedit (1.4.24-1) unstable; urgency=medium

  * Team upload
  * New upstream release 1.4.24

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Dennis Braun ]
  * d/control:
    + Bump debhelper-compat to 12
    + Bump Standards-Version to 4.4.1
  * d/copyright:
    + Update copyright year
    + Add myself for d/* 2019 copyright
  * d/patches/02-hurd_ftbfs.patch: Drop, obsolete
  * d/rules: Drop override_dh_installchangelogs, no more ChangeLog.git

 -- Dennis Braun <d_braun@kabelmail.de>  Fri, 03 Jan 2020 15:01:21 +0100

mhwaveedit (1.4.23-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Team upload.
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Gabor Karsay ]
  * Update patch to fix FTBFS on hurd
  * Bump compat level to 11
  * d/control: set Rules-Requires-Root: no
  * d/copyright: use https for URL to GNU licenses
  * d/control: bump standards version to 4.1.5
  * Add an icon for AppStream generator
  * d/copyright: add myself to copyright owners for debian folder
  * Remove unused .desktop file

 -- Gabor Karsay <gabor.karsay@gmx.at>  Thu, 26 Jul 2018 14:36:39 +0200

mhwaveedit (1.4.23-2) unstable; urgency=medium

  * Team upload.
  * Update watch file
  * Update source URLs in control and copyright
  * Use https for VCS fields
  * Bump standards version to 4.1.1
    - Remove menu
  * Bump compat level to 10
  * Remove autotools-dev
  * Enable hardening

 -- Gabor Karsay <gabor.karsay@gmx.at>  Wed, 29 Nov 2017 00:02:00 +0100

mhwaveedit (1.4.23-1) unstable; urgency=low

  * New upstream release:
    - After recording, place marks where overruns happened.
    - Playback jumping improvements for PulseAudio and JACK.
    - Make cursor movement more smooth under PulseAudio.
    - Add PulseAudio preferences for configuring latency.
    - Add option to display buffer position during playback.
    - Various bug fixes.
  * Update debian/copyright according to copyright 1.0 spec.
    Update copyright holders info.
  * Use canonical form for VCS fields.
  * Remove obsolete DM-Upload-Allowed field.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Wed, 04 Sep 2013 16:02:24 +0100

mhwaveedit (1.4.22-2) unstable; urgency=low

  * Add patch to fix FTBFS on big-endian archs.
    Thanks to Roland Stigge (Closes: #693799)

 -- Alessio Treglia <alessio@debian.org>  Sun, 25 Nov 2012 11:10:04 +0000

mhwaveedit (1.4.22-1) unstable; urgency=low

  * New upstream release:
    - Improvements and fixes in the sample conversion and dithering code
    - Display zoom and speed settings next to status bar
    - Bug fix for value truncation in position cursor dialog
    - For ogg files, choose between decompression to temporary file and
      on-the-fly access
    - Various bug fixes
  * Bump Standards.
  * Refresh patches.

 -- Alessio Treglia <alessio@debian.org>  Tue, 31 Jul 2012 23:10:58 +0200

mhwaveedit (1.4.21-2) unstable; urgency=low

  [ Alessio Treglia ]
  * Add local-options file for easy building with gbp.
  * Attempt to fix FTBFS of hurd.
  * Add lame on Recommends.

  [ Adrian Knoth ]
  * Replace hardcoded architecture list by linux-any (Closes: #634713)

 -- Alessio Treglia <alessio@debian.org>  Tue, 02 Aug 2011 17:26:31 +0200

mhwaveedit (1.4.21-1) unstable; urgency=low

  * New upstream release:
    - A few new keyboard shortcuts added for navigation.
    - In follow mode, left,right,home,end also move playback.
    - Extended playback auto-conversion to include channel and sample rate.
    - Select distance unit (seconds or samples) in position cursor dialog.
    - Bug fixes for building and wav header parsing on x86_64.
    - Various bug fixes.
    - New translation: Polish.
    - Updated translation: Italians.
  * Refresh debian/patches/01-kfreebsd_ftbfs.patch.
  * debian/rules:
    - Use 'dh $@ --with foo' syntax for compatibility with DH8.
  * Update debian/copyright file according to DEP-5 proposal (rev.174).
  * Update copyright years.
  * Small fix to the gbp config file.
  * Update Standards to 3.9.2.

 -- Alessio Treglia <alessio@debian.org>  Sun, 08 May 2011 10:20:12 +0200

mhwaveedit (1.4.20-2) unstable; urgency=low

  * Patch 01-kfreebsd_ftbfs.patch has been sent upstream.
  * Attempt to fix FTBFS on hurd.

 -- Alessio Treglia <alessio@debian.org>  Wed, 06 Oct 2010 01:56:08 +0200

mhwaveedit (1.4.20-1) unstable; urgency=low

  * New upstream release:
    - Minor changes to the manpage
    - Add manual page written by Francois Wendling.
    - ALSA driver: Implement non-event driven mode by setting
      fd count to zero
    - Alsa driver: Check maximum size and retry on EAGAIN
    - Alsa driver: Fix re-enabling of I/O group
    - alsa driver: Call snd_pcm_start if -EAGAIN error occurs in prepared
      state
    - alsa driver: Also re-enable I/O group after EAGAIN
    - ALSA driver: Also use I/O group for recording
    - ALSA driver: Use I/O groups.
    - ALSA driver: Fix period/buffer size setting
    - mainloop: Add a new IO group API
    - player: Set samplebytes properly when choosing fallback format
    - player: Make player_work into private function. Remove call from
      chunkview
  * Drop manpage, now it's provided by upstream.
  * Install ChangeLog.git as upstream's changelog.

 -- Alessio Treglia <alessio@debian.org>  Sun, 01 Aug 2010 10:56:03 +0200

mhwaveedit (1.4.19-2) unstable; urgency=low

  * Bump Standards.
  * Provide manpage for mhwaveedit (Closes: #590796, LP: #190859);
    thanks to François Wendling for the patch.

 -- Alessio Treglia <alessio@debian.org>  Thu, 29 Jul 2010 13:27:36 +0200

mhwaveedit (1.4.19-1) unstable; urgency=low

  * New upstream release:
    - Fixed bug causing cursor to freeze near end-of-file
    - Fixes for appearance in dark themes
    - Build system fixes
  * Drop the wrapper, it seems unnecessary since 1.4.14 (LP: #187552).
  * Remove /usr/lib/mhwaveedit path since the wrapper was the only file
    under that.
  * Add .gitignore file.
  * Try to fix FTBFS on kfreebsd architectures.
  * Bump Standards.
  * Update debian/copyright file.

 -- Alessio Treglia <alessio@debian.org>  Fri, 09 Jul 2010 12:26:41 +0200

mhwaveedit (1.4.18-1) unstable; urgency=low

  [ Adrian Knoth ]
  * Remove --with-default-driver=jack as suggest by upstream and use
    autodetection instead.
  * New upstream version
  * Convert package to cdbs (Closes: #549350)

  [ Alessio Treglia ]
  * Add DM-Upload-Allowed: yes.
  * Add Homepage field.
  * Switch to format 3.0 (quilt).
  * debian/copyright: Update upstream's copyright information, rewritten
    according to DEP-5 proposal.
  * Add debian/gbp.conf file.
  * Add myself to Uploaders field.
  * Switch to DH 7 + autotools_dev (Closes: #549350).
  * Drop CDBS.
  * Drop aRTs support (LP: #320915).
  * Link with --as-needed flag.
  * Update debian/watch file.
  * Move the original executable under usr/lib/mhwaveedit and install
    the wrapper in usr/bin.

 -- Alessio Treglia <alessio@debian.org>  Thu, 24 Jun 2010 09:41:01 +0200

mhwaveedit (1.4.15-2) unstable; urgency=low

  * Fixes big-endian compile error (Closes: #520152)

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Tue, 17 Mar 2009 22:57:45 +0100

mhwaveedit (1.4.15-1) unstable; urgency=low

  [Free Ekanayaka]
  * Set maintainer to Debian Multimedia Maintainers
  * Add Adrian Knoth to Uploaders
  * Add copyright notice

  [Adrian Knoth]
  * Fix "No playback for FLAC files": Tested the binary and confirmed
    it working. (Closes: #506002)
  * New upstream release (Closes: #518757)
  * Bump standards version
  * Fixed lintian warning menu-item-uses-apps-section

 -- Free Ekanayaka <freee@debian.org>  Tue, 17 Mar 2009 16:49:53 +0100

mhwaveedit (1.4.14-2) unstable; urgency=low

  * Added desktop file (Closes: #457849), thanks to Marco Rodrigues

 -- Free Ekanayaka <freee@debian.org>  Tue,  8 Jan 2008 22:20:37 +0000

mhwaveedit (1.4.14-1) unstable; urgency=low

  * New upstream release

 -- Free Ekanayaka <freee@debian.org>  Tue, 18 Dec 2007 17:02:40 +0000

mhwaveedit (1.4.13-1) unstable; urgency=low

  * New upstream release

 -- Free Ekanayaka <freee@debian.org>  Sun, 23 Sep 2007 12:10:16 +0200

mhwaveedit (1.4.11-1) unstable; urgency=low

  * New upstream release

 -- Free Ekanayaka <freee@debian.org>  Tue, 16 Jan 2007 16:52:13 +0100

mhwaveedit (1.4.9-1) unstable; urgency=low

  * New upstream release

 -- Free Ekanayaka <freee@debian.org>  Tue, 10 Oct 2006 09:52:53 +0200

mhwaveedit (1.4.7-2) unstable; urgency=low

  * Added ladspa-sdk as build dependency to enable
    support for ladspa plugins

 -- Free Ekanayaka <freee@debian.org>  Mon, 29 May 2006 10:55:55 +0200

mhwaveedit (1.4.7-1) unstable; urgency=low

  * New upstream release
  * New package maintainer Debian Multimedia Team
    <debian-multimedia@lists.debian.org>
  * Bug fix: "mhwaveedit: FTBFS on kfreebsd-amd64: unsatisfied
    Build-Depends", thanks to Petr Salinger (Closes: #361467).
  * Updated policy standard

 -- Free Ekanayaka <freee@debian.org>  Tue, 23 May 2006 07:42:55 +0200

mhwaveedit (1.4.5b-1) unstable; urgency=low

  * New upstream release
  * Depends on libartsc0-dev

 -- Free Ekanayaka <free@agnula.org>  Thu, 29 Sep 2005 09:27:02 +0100

mhwaveedit (1.4.2-2) unstable; urgency=low

  * Fixed FTBFS on GNU/kFreeBSD (closes: 327681), thanks
    to  Aurelien Jarno <aurel32@debian.org>

 -- Free Ekanayaka <free@agnula.org>  Mon, 12 Sep 2005 09:04:20 +0100

mhwaveedit (1.4.2-1) unstable; urgency=low

  * New upstream release
  * Project home page moved to gna.org, fixed watch file accordingly
  * Build depend on libjack0.100.0
  * Added missing libsamplerate and libasound build deps
  * First upload to Debian (closes #304150)

 -- Free Ekanayaka <free@agnula.org>  Thu, 28 Jul 2005 11:19:07 +0200

mhwaveedit (1.4.1-1) unstable; urgency=low

  * New upstream release

 -- Emiliano Grilli <emillo@despammed.com>  Thu,  9 Jun 2005 19:44:06 +0200

mhwaveedit (1.4.0-0demudi1) unstable; urgency=low

  * New upstream release
  * Added wrapper to set LADSPA_PATH

 -- Emiliano Grilli <emillo@despammed.com>  Fri, 27 May 2005 21:41:27 +0200

mhwaveedit (1.3.8-0.demudi1) unstable; urgency=low

  * New upstream
  * Using JACK as default

 -- Free Ekanayaka <free@agnula.org>  Mon, 25 Apr 2005 11:27:43 +0200

mhwaveedit (1.3.8pre3-0.demudi1) unstable; urgency=low

  * Added icon to the menu file
  * New upstream release
  * Use jack driver as default
  * Do not copy config.sub and config.guess

 -- Free Ekanayaka <free@agnula.org>  Tue, 12 Apr 2005 08:44:39 +0200

mhwaveedit (1.3.8pre2-1) unstable; urgency=low

  * Initial Release.

 -- Free Ekanayaka <free@agnula.org>  Sun, 10 Apr 2005 17:38:24 +0200
